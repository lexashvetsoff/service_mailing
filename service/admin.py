from django.contrib import admin

from service.models import MailingList, Client, Message, Tag, MobileOperator


admin.site.register(MailingList)
admin.site.register(Client)
admin.site.register(Message)
admin.site.register(Tag)
admin.site.register(MobileOperator)
