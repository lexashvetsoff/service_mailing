from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Tag(models.Model):
    title = models.CharField(
        max_length=200,
        verbose_name='Название тега'
    )

    class Meta:
        verbose_name = 'Тег'
        verbose_name_plural = 'Теги'

    def __str__(self):
        return self.title


class MobileOperator(models.Model):
    title = models.CharField(
        max_length=200,
        verbose_name='Название оператора'
    )

    code = models.PositiveSmallIntegerField(
        verbose_name='Код оператора'
    )

    class Meta:
        verbose_name = 'Мобильный оператор'
        verbose_name_plural = 'Мобильные операторы'

    def __str__(self):
        return f'{self.title}({self.code})'


class MailingList(models.Model):
    title = models.CharField(
        max_length=200,
        verbose_name='Название рассылки',
        default='first'
    )

    start = models.DateTimeField(
        verbose_name='Старт рассылки'
    )

    message_text = models.TextField(
        verbose_name='Текст сообщения'
    )

    filter_by_tag = models.ForeignKey(
        Tag,
        verbose_name='Фильтровать рассылку по тегу',
        on_delete=models.CASCADE,
        related_name='filter_by_tags',
        blank=True,
        null=True,
        default=''
    )

    filter_by_code = models.ForeignKey(
        MobileOperator,
        verbose_name='Фильтровать рассылку по мобильному оператору',
        on_delete=models.CASCADE,
        related_name='filter_by_codes',
        blank=True,
        null=True,
        default=''
    )

    end = models.DateTimeField(
        verbose_name='Окончание рассылки'
    )

    status = models.BooleanField(
        verbose_name='Исполнение рассылки',
        default=False
    )

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'
    
    def __str__(self):
        return self.title


class Client(models.Model):
    phon = PhoneNumberField(
        verbose_name='Номер телефона'
    )

    mobile_operator_code = models.ForeignKey(
        MobileOperator,
        verbose_name='Мобильный оператор',
        on_delete=models.CASCADE,
        related_name='mobile_operators',
    )

    tag = models.ForeignKey(
        Tag,
        verbose_name='Тег',
        on_delete=models.CASCADE,
        related_name='tags'
    )

    timezone = models.PositiveSmallIntegerField(
        verbose_name='Часовой пояс (МСК +)'
    )

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'
    
    def __str__(self):
        return str(self.phon)


class Message(models.Model):
    message_date = models.DateTimeField(
        verbose_name='Дата и время отправки сообщения'
    )
    status = models.BooleanField(
        verbose_name='Статус отправки',
        blank=True,
        default=False
    )

    mailing_list = models.ForeignKey(
        MailingList,
        verbose_name='Рассылка',
        on_delete=models.CASCADE,
        related_name='mailing_lists'
    )

    client = models.ForeignKey(
        Client,
        verbose_name='Клиент',
        on_delete=models.CASCADE,
        related_name='clients'
    )

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
    
    def __str__(self):
        return f'{self.message_date} для {self.client}'
