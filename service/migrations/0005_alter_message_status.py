# Generated by Django 4.1 on 2022-08-23 18:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0004_alter_message_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='status',
            field=models.BooleanField(blank=True, default=False, verbose_name='Статус отправки'),
        ),
    ]
