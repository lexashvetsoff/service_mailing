from django.core.management.base import BaseCommand

from service.scripts import main_script


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        main_script.main()