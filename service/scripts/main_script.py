from service import models
from environs import Env
import requests
from datetime import datetime
import pytz
from time import sleep


def main():
    env = Env()
    env.read_env()

    token_api = env('TOKEN_API')

    time_zone = pytz.timezone('UTC')
    now = datetime.now(time_zone)
    print(now)

    while True:
        mailing_lists = models.MailingList.objects.filter(status=False)

        for mailing_list in mailing_lists:
            if mailing_list.start <= now:
                if now > mailing_list.end:
                    continue

                if not mailing_list.filter_by_tag and not mailing_list.filter_by_code:
                    clients = models.Client.objects.all()
                
                elif mailing_list.filter_by_tag and mailing_list.filter_by_code:
                    clients = models.Client.objects.filter(tag=mailing_list.filter_by_tag, mobile_operator_code=mailing_list.filter_by_code)
                
                elif mailing_list.filter_by_tag:
                    clients = models.Client.objects.filter(tag=mailing_list.filter_by_tag)
                
                elif mailing_list.filter_by_code:
                    clients = models.Client.objects.filter(tmobile_operator_code=mailing_list.filter_by_code)

                for client in clients:
                    new_message, created = models.Message.objects.get_or_create(
                        message_date=datetime.now(time_zone),
                        mailing_list=mailing_list,
                        client=client
                    )
                    if created:
                        data = {
                            "id": new_message.id,
                            "phone": int(f'+{client.phon.country_code}{client.phon.national_number}'),
                            "text": mailing_list.message_text
                        }

                        headers = {
                            'Authorization': f'Bearer {token_api}',
                        }

                        url = f'https://probe.fbrq.cloud/v1/send/{new_message.id}'
                        try:
                            response = requests.post(url, headers=headers, data=data)
                            response.raise_for_status()
                        except Exception:
                            pass

                        if response.status_code == 200:
                            new_message.status = True
                            new_message.save()
                
                mailing_list.status = True
                mailing_list.save()
        
        sleep(100)


if __name__ == '__main__':
    main()